﻿using ExampleApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using System.Threading.Tasks;
using Plugin.Permissions;
using System.Reflection;
using Android.Webkit;
using Android.Support.V4.Content;

namespace ExampleApp.Droid.Services
{
    public class FileService : IFileService
    {
        public async Task<string> ExtractEmbeddedResourceAsync(string resourceAssembly, string outputFileName)
        {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData);
            path = Path.Combine(path, outputFileName);

            if (!File.Exists(path))
            {
                var assembly = IntrospectionExtensions.GetTypeInfo(typeof(ExampleApp.App)).Assembly;
                using (var resource = assembly.GetManifestResourceStream(resourceAssembly))
                {
                    using (var file = new FileStream(path, FileMode.Create, FileAccess.Write))
                    {
                        resource.CopyTo(file);
                    }
                }
            }

            return path;
        }

        public async Task OpenFileForEditFromPathAsync(string filePath)
        {
            Java.IO.File file = new Java.IO.File(filePath);
            var exists = file.Exists();

            Android.Net.Uri path = Android.Net.Uri.FromFile(file);
            string extension = MimeTypeMap.GetFileExtensionFromUrl(Android.Net.Uri.FromFile(file).ToString());
            string mimeType = MimeTypeMap.Singleton.GetMimeTypeFromExtension(extension);
            Intent intent = new Intent(Intent.ActionEdit);
            intent.AddFlags(ActivityFlags.ClearWhenTaskReset);
            intent.AddFlags(ActivityFlags.GrantReadUriPermission);
            intent.AddFlags(ActivityFlags.GrantWriteUriPermission);
            intent.AddFlags(ActivityFlags.GrantPersistableUriPermission);


            var uri = FileProvider.GetUriForFile(Android.App.Application.Context, Android.App.Application.Context.PackageName + ".fileprovider", file);

            if (uri != null)
            {
                intent.SetData(uri);
            }

            this.StartActivity(Intent.CreateChooser(intent, "Choose App"));
        }
    }

    public static class ObjectExtensions
    {
        public static void StartActivity(this object o, Intent intent)
        {
            var context = o as Context;
            if (context != null)
                context.StartActivity(intent);
            else
            {
                intent.SetFlags(ActivityFlags.NewTask);
                Application.Context.StartActivity(intent);
            }
        }
    }
}