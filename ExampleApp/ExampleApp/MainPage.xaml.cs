﻿using ExampleApp.Boostrap;
using ExampleApp.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ExampleApp
{
    public partial class MainPage : ContentPage
    {
        private readonly IFileService fileService;

        public MainPage()
        {
            InitializeComponent();

            fileService = AppContainer.Resolve<IFileService>();
        }

        private async void LoadJpg_Clicked(object sender, EventArgs e)
        {
            string filePath = await fileService.ExtractEmbeddedResourceAsync("ExampleApp.EmbeddedResources.TestJpg.jpg", "JpgFile.jpg");

            await fileService.OpenFileForEditFromPathAsync(filePath);
        }

        private async void LoadPdf_Clicked(object sender, EventArgs e)
        {
            string filePath = await fileService.ExtractEmbeddedResourceAsync("ExampleApp.EmbeddedResources.TestPdf.pdf", "PdfFile.pdf");

            await fileService.OpenFileForEditFromPathAsync(filePath);
        }
    }
}
