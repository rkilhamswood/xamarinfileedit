﻿
using System.IO;
using System.Threading.Tasks;

namespace ExampleApp.Interfaces
{
    public interface IFileService
    {
        Task<string> ExtractEmbeddedResourceAsync(string resourceAssembly, string outputFileName);
        Task OpenFileForEditFromPathAsync(string filePath);
    }
}
