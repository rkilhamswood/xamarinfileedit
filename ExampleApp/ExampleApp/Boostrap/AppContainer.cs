﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleApp.Boostrap
{
    public class AppContainer
    {
        private static IContainer container;

        public static void RegisterDependencies(ContainerBuilder builder = null)
        {
            if (builder == null)
            {
                builder = new ContainerBuilder();
            }

            container = builder.Build();
        }

        public static object Resolve(Type typeName)
        {
            return container.Resolve(typeName);
        }

        public static T Resolve<T>()
        {
            return container.Resolve<T>();
        }
    }
}
